<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['username' => 'SevenDark', 'phone' => '09335136797', 'password' => 'SevenDark', 'phone_verified_at' => now()],
            ['username' => 'SevenEncoder', 'phone' => '09941929362', 'password' => 'SevenDark', 'phone_verified_at' => now()],
            ['username' => 'AbediMehdi', 'phone' => '09123456789', 'password' => 'SevenDark', 'phone_verified_at' => now()],
            ['username' => 'AliAhmadi', 'phone' => '09987654321', 'password' => 'SevenDark', 'phone_verified_at' => now()],
        ];
    }
}
