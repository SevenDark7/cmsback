<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user')->group(function () {
    include __DIR__ . '/user/auth.php';
    include __DIR__ . '/user/task.php';
});

Route::prefix('admin')->middleware(['auth:sanctum', 'abilities:admin'])->group(function () {
    include __DIR__ . '/admin/task.php';
});
