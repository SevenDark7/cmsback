<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UserTaskTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_tasks()
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user);
        $response = $this->get('/api/user/tasks');

        $response->assertStatus(200);
    }

    public function test_store_user_task()
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user);
        $this->postJson('/api/user/tasks', [
            'title' => 'test',
            'description' => 'test description',
            'status' => 1
        ])->assertStatus(201);
    }

    public function test_show_user_task()
    {
        $user = User::factory()->create();
        $task = Task::factory()->create([
            'user_id' => $user->id
        ]);
        Sanctum::actingAs($user);
        $this->get('/api/user/tasks/' . $task->id)->assertStatus(200);
    }

    public function test_update_user_task()
    {
        $user = User::factory()->create();
        $task = Task::factory()->create([
            'user_id' => $user->id
        ]);
        Sanctum::actingAs($user);
        $this->putJson('/api/user/tasks/' . $task->id, [
            'title' => 'Test Title',
            'description' => $task->description,
            'status' => 3
        ])->assertStatus(200);

        $task->refresh();

        $this->assertSame('Test Title', $task->title);
    }

    public function test_destory_user_task()
    {
        $user = User::factory()->create();
        $task = Task::factory()->create([
            'user_id' => $user->id
        ]);
        Sanctum::actingAs($user);
        $this->deleteJson('/api/user/tasks/' . $task->id)->assertStatus(200);
        $this->assertFalse(Task::where('user_id', $user->id)->exists());
    }

    public function test_delete_other_user_task()
    {
        $user = User::factory()->create();
        $task = Task::factory()->create();
        Sanctum::actingAs($user);
        $this->deleteJson('/api/user/tasks/' . $task->id)->assertStatus(422);
        $this->assertTrue(Task::where('id', $task->id)->exists());
    }
}
