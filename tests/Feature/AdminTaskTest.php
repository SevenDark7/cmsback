<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AdminTaskTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_all_task_by_admin()
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user, ['admin']);
        $response = $this->postJson('api/admin/tasks/all');

        $response->assertStatus(200);
    }

    public function test_get_all_task_use_fDate()
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user, ['admin']);
        $response = $this->postJson('api/admin/tasks/all', [
            'fDate' => '2022-03-19',
            'tDate' => '2022-05-03',
        ]);

        $response->assertStatus(200);
    }

    public function test_get_all_task_use_order()
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user, ['admin']);
        $response = $this->postJson('api/admin/tasks/all', [
            'order' => 'DESC'
        ]);

        $response->assertStatus(200);
    }

    public function test_get_all_task_by_user()
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user);
        $response = $this->postJson('api/admin/tasks/all');

        $response->assertStatus(403);
    }

    public function test_store_task_by_admin()
    {
        $user = User::factory()->create();
        $admin = User::factory()->create();
        Sanctum::actingAs($admin, ['admin']);
        $this->postJson('/api/admin/tasks', [
            'title' => 'test',
            'description' => 'test description',
            'user_id' => $user->id,
            'status' => 1
        ])->assertStatus(201);
    }

    public function test_show_task_by_admin()
    {
        $user = User::factory()->create();
        $task = Task::factory()->create();
        Sanctum::actingAs($user, ['admin']);
        $this->get('/api/admin/tasks/' . $task->id)->assertStatus(200);
    }

    public function test_update_task_by_admin()
    {
        $user = User::factory()->create();
        $task = Task::factory()->create();
        Sanctum::actingAs($user, ['admin']);
        $this->putJson('/api/admin/tasks/' . $task->id, [
            'title' => 'Test Title',
            'description' => $task->description,
            'user_id' => $task->user_id,
            'status' => 3
        ])->assertStatus(200);

        $task->refresh();

        $this->assertSame('Test Title', $task->title);
    }

    public function test_destory_task_by_admin()
    {
        $user = User::factory()->create();
        $task = Task::factory()->create();
        Sanctum::actingAs($user, ['admin']);
        $this->deleteJson('/api/admin/tasks/' . $task->id)->assertStatus(200);
        $this->assertFalse(Task::where('user_id', $user->id)->exists());
    }

}
