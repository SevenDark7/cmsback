<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_info()
    {
        Sanctum::actingAs(User::factory()->create());
        $response = $this->get('api/user/auth');

        $response->assertStatus(200);
    }

    public function test_register_user()
    {
        $response = $this->postJson('api/user/auth/register', [
            'username' => 'SevenDark',
            'phone' => '09335136797',
            'password' => 'SevenDark'
        ]);

        $response->assertStatus(201);
    }

    public function test_login_user()
    {
        $user = User::factory()->create();
        $response = $this->postJson('api/user/auth/login', [
            'username' => $user->username,
            'password' => $user->password
        ]);

        $response->assertStatus(200);
    }

    public function test_login_user_error()
    {
        $user = User::factory()->create();
        $response = $this->postJson('api/user/auth/login', [
            'username' => $user->username,
            'password' => 'testtest'
        ]);

        $response->assertStatus(422);
    }
}
