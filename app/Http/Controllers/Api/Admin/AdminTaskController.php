<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Repositories\Admin\AdminTaskRepository;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class AdminTaskController extends Controller
{

    protected $repo;


    public function __construct()
    {
        $this->repo = resolve(AdminTaskRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $request->validate([
            'fDate' => ['sometimes'],
            'tDate' => ['sometimes'],
            'order' => ['sometimes'],
        ]);
        $tasks = $this->repo->index($request);
        return response()->json($tasks, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'title' => ['required'],
            'description' => ['required', 'min:5'],
            'status' => ['required'],
            'user_id' => ['required'],
            'meta' => ['nullable']
        ]);

        $this->repo->store($request);
        return response()->json([
            'message' => 'User Report Submitted Successfully'
        ], Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Task $task
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, Task $task): JsonResponse
    {
        $request->validate([
            'title' => ['required'],
            'description' => ['required', 'min:5'],
            'status' => ['required'],
            'user_id' => ['required'],
            'meta' => ['nullable']
        ]);

        $this->repo->update($request, $task);
        return \response()->json([
            'message' => 'User Report Updated Successfully'
        ], Response::HTTP_OK);
    }
}
