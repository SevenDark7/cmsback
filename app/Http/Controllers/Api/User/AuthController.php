<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Repositories\User\AuthRepository;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{

    protected $repo;
    protected $admin = '09335136797';

    public function __construct()
    {
        $this->repo = resolve(AuthRepository::class);
    }


    /**
     * Register New User
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        $request->validate([
            'username' => ['required', 'min:5', 'max:30', 'regex:/(^([a-zA-z]+)(\d+)?$)/u', 'unique:users'],
            'phone' => ['required', 'min:11', 'max:11', 'regex:/(09)[0-9]{9}/', 'unique:users'],
            'password' => ['required', 'min:8', 'max:20'],
        ]);

        $user = $this->repo->store($request);

        return response()->json([
            'user' => $user->phone == $this->admin ? $user->createToken('userToken', ['admin'])
                ->plainTextToken : $user->createToken('userToken')->plainTextToken,
            'message' => 'User Created Successfully'
        ], Response::HTTP_CREATED);
    }

    /**
     * Login User With Username And Password
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request): JsonResponse
    {
        $request->validate([
            'username' => ['required'],
            'password' => ['required']
        ]);

        // Check User Credentials For Login
        if ($user = User::where([['username', $request->username], ['password', $request->password]])->first()) {
            $data = [
                'user' => $user->phone == $this->admin ? $user->createToken('userToken', ['admin'])
                    ->plainTextToken : $user->createToken('userToken')->plainTextToken,
                'message' => 'User Logged In Successfully'
            ];
            return response()->json($data, Response::HTTP_OK);
        }

        throw ValidationException::withMessages([
            'username' => 'Incorrect Username'
        ]);
    }

    /**
     * Return A User Logged In Info
     * @return JsonResponse
     */
    public function user(): JsonResponse
    {
        $data = [
            'user' => $this->repo->user(),
            'message' => 'Authenticated'
        ];
        return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Logout A User
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $request->user()->tokens()->delete();
        return response()->json([
            'message' => 'User Logged Out Successfully'
        ], Response::HTTP_OK);
    }
}
