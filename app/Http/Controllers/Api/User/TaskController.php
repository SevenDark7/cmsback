<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Repositories\User\TaskRepository;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\ValidationException;

class TaskController extends Controller
{

    protected $repo;


    public function __construct()
    {
        $this->repo = resolve(TaskRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $tasks = $this->repo->index();
        return response()->json($tasks, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'title' => ['required'],
            'description' => ['required', 'min:5'],
            'status' => ['required'],
            'meta' => ['nullable']
        ]);

        $this->repo->store($request);
        return response()->json([
            'message' => 'User Report Submitted Successfully'
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Task $task
     * @return JsonResponse
     * @throws ValidationException
     */
    public function show(Task $task): JsonResponse
    {
        if (Gate::forUser(Auth::user())->allows('task-manage', $task)) {
            $taskInfo = $this->repo->show($task);
            return response()->json($taskInfo, Response::HTTP_OK);
        }

        throw ValidationException::withMessages([
            'message' => 'Access Denied'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Task $task
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, Task $task): JsonResponse
    {
        $request->validate([
            'title' => ['required'],
            'description' => ['required', 'min:5'],
            'status' => ['required'],
            'meta' => ['nullable']
        ]);

        if (Gate::forUser(Auth::user())->allows('task-manage', $task)) {
            $this->repo->update($request, $task);
            return \response()->json([
                'message' => 'User Report Updated Successfully'
            ], Response::HTTP_OK);
        }

        throw ValidationException::withMessages([
            'message' => 'Access Denied'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Task $task
     * @return JsonResponse
     * @throws ValidationException
     */
    public function destroy(Task $task): JsonResponse
    {
        if (Gate::forUser(Auth::user())->allows('task-manage', $task)) {
            $this->repo->destroy($task);
            return \response()->json([
                'message' => 'User Report Deleted Successfully'
            ], Response::HTTP_OK);
        }

        throw ValidationException::withMessages([
            'message' => 'Access Denied'
        ]);
    }
}
