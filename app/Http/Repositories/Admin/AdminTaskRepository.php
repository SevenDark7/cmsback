<?php

namespace App\Http\Repositories\Admin;

use App\Models\Task;
use Illuminate\Http\Request;

class AdminTaskRepository
{

    /**
     * Return All Tasks To Admin
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return Task::query()->when(isset($request->fDate), function ($query) use ($request) {
            $query->where('created_at', '>=', $request->fDate);
        })->when(isset($request->tDate), function ($query) use ($request) {
            $query->where('created_at', '<=', $request->tDate);
        })->when(isset($request->order), function ($query) use ($request) {
            $query->orderBy('status', $request->order);
        })->where('active', 1)->withTrashed()->get();
    }

    /**
     * Add New Task Report By Admin
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        Task::create([
            'title' => $request->title,
            'description' => $request->description,
            'status' => $request->status,
            'user_id' => $request->user_id,
            'meta' => $request->meta
        ]);
    }

    /**
     * Update User Task By Admin
     * @param Request $request
     * @param Task $task
     * @return void
     */
    public function update(Request $request, Task $task)
    {
        $task->update([
            'title' => $request->title,
            'description' => $request->description,
            'status' => $request->status,
            'user_id' => $request->user_id,
            'meta' => $request->meta
        ]);
    }
}
