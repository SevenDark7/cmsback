<?php

namespace App\Http\Repositories\User;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskRepository
{

    /**
     * Return Tasks Of User With Removed User Tasks
     * @return mixed
     */
    public function index()
    {
        return Auth::user()->tasks()->withTrashed()->get();
    }

    /**
     * Add New Task Report By User
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        Task::create([
            'title' => $request->title,
            'description' => $request->description,
            'status' => $request->status,
            'user_id' => Auth::id(),
            'meta' => $request->meta
        ]);
    }

    /**
     * Return A Task Info
     * @param Task $task
     * @return mixed
     */
    public function show(Task $task)
    {
        return Task::find($task->id);
    }

    /**
     * Update User Task By User
     * @param Request $request
     * @param Task $task
     * @return void
     */
    public function update(Request $request, Task $task)
    {
        $task->update([
            'title' => $request->title,
            'description' => $request->description,
            'status' => $request->status,
            'meta' => $request->meta
        ]);
    }

    /**
     * Soft Delete A Task Of A User
     * @param Task $task
     * @return void
     */
    public function destroy(Task $task)
    {
        $task->delete();
    }
}
