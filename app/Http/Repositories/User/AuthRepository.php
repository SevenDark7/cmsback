<?php

namespace App\Http\Repositories\User;

use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthRepository
{

    /**
     * Return User Info
     * @return Authenticatable|null
     */
    public function user(): ?Authenticatable
    {
        return Auth::user();
    }

    /**
     * Register New User
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return User::create([
            'username' => $request->username,
            'phone' => $request->phone,
            'password' => $request->password,
            'phone_verified_at' => now()
        ]);
    }
}
